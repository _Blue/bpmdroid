#! /bin/bash

set -ue

ABI_VERSION=29

function setup()
{
  bash -c "cd deps/BluePasswordManager && tools/android-ci.sh $1 $2$ABI_VERSION $3 $4"

  mkdir -p "app/jniLibs/$5"
  cp -v "deps/BluePasswordManager/lib/libbpm.so" "app/jniLibs/$5"
  cp -v "deps/BluePasswordManager/deps/cryptopp/libcryptopp.so" "app/jniLibs/$5"
  cp -v "/tmp/bpm/ndk-instance-$1/sysroot/usr/lib/$2/29/libm.so" "app/jniLibs/$5"
  cp -v "/tmp/bpm/ndk-instance-$1/sysroot/usr/lib/$2/29/libdl.so" "app/jniLibs/$5"
  cp -v "/tmp/bpm/ndk-instance-$1/sysroot/usr/lib/$4/libc++_shared.so" "app/jniLibs/$5"
  cp -v "/tmp/bpm/ndk-instance-$1/sysroot/usr/lib/$2/29/libc.so" "app/jniLibs/$5"


}

setup arm64 aarch64-linux-android arm64_v8a aarch64-linux-android arm64-v8a
setup x86 i686-linux-android i686 i686-linux-android x86
setup x86_64 x86_64-linux-android x86_64 x86_64-linux-android x86_64
