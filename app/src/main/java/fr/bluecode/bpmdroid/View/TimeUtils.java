package fr.bluecode.bpmdroid.View;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils
{
  public static String Format(long ts)
  {
    return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Date(ts * 1000));
  }

  public static Long Now()
  {
    return new Date().getTime() / 1000;
  }
}
