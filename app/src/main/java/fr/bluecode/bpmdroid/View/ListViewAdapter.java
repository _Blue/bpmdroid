package fr.bluecode.bpmdroid.View;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.bluecode.bpmdroid.R;
import fr.bluecode.bpmdroid.storage.Credentials;

public class ListViewAdapter extends BaseAdapter
{
  private List<Credentials> _credentials;
  private List<View> _items;
  private List<View> _reusable_items; // A pool of already built views (used to optimize list refresh performance)
  private Activity _context;

  public ListViewAdapter(List<Credentials> credentials, Activity context)
  {
    _context = context;
    _reusable_items = new ArrayList<>();

    SetCredentials(credentials);
  }

  public void SetCredentials(List<Credentials> credentials)
  {
    _credentials = credentials;

    _items = new ArrayList<>();

    for (int i = 0; i < _credentials.size(); i++)
    {
      View reusable_view = i >= _reusable_items.size() ? null : _reusable_items.get(i);

      _items.add(CreateView(_credentials.get(i), reusable_view));
    }

    // recycle the previously used views
    _reusable_items = _reusable_items.size() > _items.size() ? _reusable_items : _items;
  }

  private static void SetControlText(View view, int id, String text)
  {
    TextView text_view = view.findViewById(id);

    text_view.setText(text);
  }

  private View CreateView(Credentials credentials, View reusable_view)
  {
    View view;
    if (reusable_view != null) // Reuse existing view if possible
    {
      view = reusable_view;
    }
    else // else create a new one
    {
      view = _context.getLayoutInflater().inflate(R.layout.credentials_view, null);
    }

    SetControlText(view, R.id.domain, credentials.Domain());
    SetControlText(view, R.id.username, credentials.Username());
    SetControlText(view, R.id.date, TimeUtils.Format(credentials.Timestamp()));
    view.setTag(credentials);

    return view;
  }

  @Override
  public int getCount()
  {
    return _credentials.size();
  }

  @Override
  public Object getItem(int position)
  {
    return _credentials.get(position);
  }

  @Override
  public long getItemId(int position)
  {
    return 0;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent)
  {
    return _items.get(position);
  }
}
