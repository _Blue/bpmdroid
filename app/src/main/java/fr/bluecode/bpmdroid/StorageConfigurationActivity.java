package fr.bluecode.bpmdroid;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import fr.bluecode.bpmdroid.storage.PathUtils;
import fr.bluecode.bpmdroid.storage.Storage;

public class StorageConfigurationActivity extends Activity implements TextWatcher
{
  public enum Result
  {
    NotConfigured,
    Configured
  }

  private Options _options;
  private TextView _path;
  private TextView _bad_password_notice;
  private TextView _password;
  private Button _select;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_storage_configuration);
    _options = new Options(this);

    LoadControls();
    CheckStoragePermissionsGranted();
    Refresh();

    if (!_options.HasStoragePathSet())
    {
      AskForStoragePath();
    }
  }

  @Override
  protected void onResume()
  {
    super.onResume();

    _password.requestFocus();

    // Make the keyboard appear
    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

    InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    manager.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
  }

  private void CheckStoragePermissionsGranted()
  {
    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
  }

  private void LoadControls()
  {
    _path = findViewById(R.id.path);
    _bad_password_notice = findViewById(R.id.bad_password_notice);
    _password = findViewById(R.id.password);
    _select = findViewById(R.id.select);
    _select.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        AskForStoragePath();
      }
    });

    _password.addTextChangedListener(this);
  }

  private Pair<Boolean, String> IsPasswordValid()
  {
    try
    {
      Storage storage = Storage.Create(_path.getText().toString(), _password.getText().toString());
      return Pair.create(true, null);
    }
    catch (Exception e)
    {
      Log.e("storage", "Failed to open storage: " + e);

      return Pair.create(false, e.getMessage());
    }
  }

  private void Refresh()
  {
    if (_options.HasStoragePathSet())
    {
      _path.setText(_options.GetStoragePath());
    }

    if (_options.HasStoragePathSet())
    {
      Pair<Boolean, String> storage_unlock_result = IsPasswordValid();
      if (storage_unlock_result.first)
      {
        _bad_password_notice.setText("Successfully unlocked storage");
        _bad_password_notice.setTextColor(Color.GREEN);
        Intent intent = new Intent();
        intent.putExtra("storage", Storage.Create(_path.getText().toString(), _password.getText().toString()));
        setResult(Result.Configured.ordinal(), intent);
        finish();
        return;
      }
      else
      {
        _bad_password_notice.setText(storage_unlock_result.second);
      }
    }
    else
    {
      _bad_password_notice.setText("Please select a file to open");
    }
    _bad_password_notice.setTextColor(Color.RED);
  }

  private void AskForStoragePath()
  {
    Intent intent = new Intent().setType("*/*").setAction(Intent.ACTION_GET_CONTENT);

    startActivityForResult(Intent.createChooser(intent, "Select a storage file to open"), 0);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == 0 && resultCode == RESULT_OK)
    {
      Uri uri = data.getData();

      _options.SetStoragePath(PathUtils.PathFromUri(uri, this));

      Log.v("main", "Selected storage file: " + Objects.requireNonNull(uri).getPath());
      Refresh();
    }
  }

  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after)
  {

  }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count)
  {
    Refresh();
  }

  @Override
  public void afterTextChanged(Editable s)
  {

  }
}
