package fr.bluecode.bpmdroid.storage;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

public class PathUtils
{
  public static String PathFromUri(Uri uri, Context context)
  {
    if (!DocumentsContract.isDocumentUri(context, uri))
    {
      return null;
    }

    if ("com.android.externalstorage.documents".equals(uri.getAuthority()))
    {
      return Environment.getExternalStorageDirectory() + "/" + DocumentsContract.getDocumentId(uri).split(":")[1];
    }

    String id = DocumentsContract.getRootId(uri);

    Cursor cursor = context.getContentResolver().query(uri, null, "_id=?", new String[]{id}, null);
    if (cursor == null || !cursor.moveToFirst())
    {
      throw new RuntimeException("Failed to get absolute path for: " + uri.getPath());
    }

    try
    {
      return cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));
    }
    finally
    {
      cursor.close();
    }
  }
}
