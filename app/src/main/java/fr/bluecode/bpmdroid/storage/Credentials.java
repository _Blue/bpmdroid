package fr.bluecode.bpmdroid.storage;

import java.io.Serializable;

public class Credentials implements Serializable
{
  private String _domain;
  private String _username;
  private String _password;
  private long _ts;

  private boolean _symlink;

  public Credentials(String domain, String username, String password, long ts, boolean symlink)
  {
    _domain = domain;
    _username = username;
    _password = password;
    _ts = ts;
    _symlink = symlink;
  }

  public Credentials(String domain, String username, long ts)
  {
    _domain = domain;
    _username = username;
    _ts = ts;
  }

  public String Domain()
  {
    return _domain;
  }

  public String Username()
  {
    return _username;
  }

  public String Password()
  {
    return _password;
  }

  public long Timestamp()
  {
    return _ts;
  }

  public boolean Symlink() { return _symlink; };

  public boolean MatchesPattern(String input_pattern)
  {
    String pattern = input_pattern.toLowerCase();

    return _domain.toLowerCase().contains(pattern)
           || _username.toLowerCase().contains(pattern)
           || (_password != null && _password.contains(pattern));
  }

  public boolean HasPassword()
  {
    return _password != null;
  }
}