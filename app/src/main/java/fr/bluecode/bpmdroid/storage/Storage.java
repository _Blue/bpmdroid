package fr.bluecode.bpmdroid.storage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Storage implements AutoCloseable, Serializable
{
  private long _storage = 0; // Underlying storage pointer

  private Storage(long storage)
  {
    _storage = storage;
  }

  public static Storage Create(String path, String password)
  {
    long ptr = CreateImpl(path, password);

    return new Storage(ptr);
  }


  private native Credentials[] GetCredentialsImpl();

  public List<Credentials> GetCredentials()
  {
    return Arrays.asList(GetCredentialsImpl());
  }

  private native void ReleaseImpl() throws Exception;


  public native boolean AddCredentials(String domain, String username, String password, long ts, boolean overwrite);

  public native void EditUsername(String domain, String new_username, long ts);

  public native void EditDomain(String old_domain, String new_domain, long ts);

  public native void EditPassword(String domain, String new_password, long ts);

  public native void DeleteCredentials(String domain);

  private static native long CreateImpl(String path, String password);

  public static native String GetAuthorityFromUrl(String url);

  @Override
  public void close() throws Exception
  {
    ReleaseImpl();
    _storage = 0;
  }
}
