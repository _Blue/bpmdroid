package fr.bluecode.bpmdroid.storage;

import android.util.Log;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CredentialsView implements Serializable, AutoCloseable
{
  private Storage _storage;
  private List<Credentials> _credentials;

  private LocalDateTime _expiration_ts = null;

  public CredentialsView(Storage storage)
  {
    _storage = storage;
    Refresh();
  }

  public void Refresh()
  {
    _credentials = _storage.GetCredentials();
  }

  public List<Credentials> GetFilteredView(final String pattern)
  {
    ArrayList<Credentials> output = new ArrayList<>();
    for (Credentials e : _credentials)
    {
      if (e.MatchesPattern(pattern))
      {
        output.add(e);
      }
    }

    return output;
  }

  public void UpdateCredentials(Credentials old_credentials, Credentials new_credentials)
  {
    boolean domain_change = !old_credentials.Domain().equals(new_credentials.Domain());
    boolean username_change = !old_credentials.Username().equals(new_credentials.Username());
    boolean password_change = !old_credentials.Password().equals(new_credentials.Password());

    if (domain_change)
    {
      _storage.EditDomain(old_credentials.Domain(), new_credentials.Domain(), new_credentials.Timestamp());
    }

    if (username_change)
    {
      _storage.EditUsername(new_credentials.Domain(), new_credentials.Username(), new_credentials.Timestamp());
    }

    if (password_change)
    {
      _storage.EditPassword(new_credentials.Domain(), new_credentials.Password(), new_credentials.Timestamp());
    }

    Refresh();
  }

  public void DeleteCredentials(Credentials credentials)
  {
    _storage.DeleteCredentials(credentials.Domain());
    Refresh();
  }

  public boolean AddCredentials(Credentials credentials, boolean overwrite)
  {
    boolean added = _storage.AddCredentials(credentials.Domain(), credentials.Username(), credentials.Password(), credentials.Timestamp(), overwrite);

    Refresh();

    return added;
  }

  public List<Credentials> GetAll()
  {
    return _credentials;
  }

  @Override
  public void close() throws Exception
  {
    _storage.close();
  }

  public boolean IsExpired()
  {
    return _expiration_ts != null && _expiration_ts.isBefore(LocalDateTime.now());
  }

  public void SetExpirationTs(LocalDateTime ts)
  {
    Log.i("StorageView", "Expiration ts set to: " + ts);
    _expiration_ts = ts;
  }
}
