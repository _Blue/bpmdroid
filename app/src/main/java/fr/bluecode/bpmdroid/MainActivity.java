package fr.bluecode.bpmdroid;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Predicate;

import fr.bluecode.bpmdroid.View.ListViewAdapter;
import fr.bluecode.bpmdroid.storage.Credentials;
import fr.bluecode.bpmdroid.storage.CredentialsView;
import fr.bluecode.bpmdroid.storage.Storage;

public class MainActivity extends AppCompatActivity implements TextWatcher, AdapterView.OnItemClickListener
{
  static
  {
    System.loadLibrary("native-lib");
  }

  private enum Flows
  {
    ConfigureStorage,
    DisplayCredentials,
    FollowSymlink
  }

  private ListView _list;
  private ListViewAdapter _adapter;
  private CredentialsView _credentials_view;
  private EditText _filter;
  private Button _configure;
  private Button _create;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    SetupControls();


    // Recover any saved storage view
    if (savedInstanceState != null && savedInstanceState.containsKey("credentials_view"))
    {
      _credentials_view = (CredentialsView)savedInstanceState.getSerializable("credentials_view");
    }
  }

  @Override
  public void onResume()
  {
    super.onResume();

    _list.requestFocus();

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && !Environment.isExternalStorageManager())
    {
      startActivity(new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION, Uri.parse("package:" + BuildConfig.APPLICATION_ID)));
      Log.i("MainActivity", "Requesting storage manager access");
    }

    if (_credentials_view == null || _credentials_view.IsExpired())
    {
      ConfigureStorage();
    }
  }

  @Override
  public void onSaveInstanceState(Bundle bundle)
  {
    super.onSaveInstanceState(bundle);

    if (!isFinishing() && _credentials_view != null)
    {
      _credentials_view.SetExpirationTs(LocalDateTime.now().plusMinutes(10));
      bundle.putSerializable("credentials_view", _credentials_view);
    }
  }

  @Override
  public void onDestroy()
  {
    if (_credentials_view != null && isFinishing())
    {
      try
      {
        _credentials_view.close();
      }
      catch (Exception e)
      {
        Log.e("main", e.toString());
      }
    }
    super.onDestroy();
  }

  private void SetupControls()
  {
    _adapter = new ListViewAdapter(new ArrayList<Credentials>(), this);
    _list = findViewById(R.id.gridview);
    _list.setAdapter(_adapter);

    _filter = findViewById(R.id.filter);
    _filter.addTextChangedListener(this);

    _list.setOnItemClickListener(this);

    _configure = findViewById(R.id.update_storage);
    _configure.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        ConfigureStorage();
      }
    });

    _create = findViewById(R.id.add_credentials);
    _create.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        CreateCredentials();
      }
    });
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == Flows.DisplayCredentials.ordinal())
    {
      OnCredentialsDisplayResult(CredendialsViewActivity.Result.values()[resultCode], data);
      Refresh();
      return;
    }

    if (requestCode == Flows.ConfigureStorage.ordinal())
    {
      if (resultCode == StorageConfigurationActivity.Result.Configured.ordinal())
      {
        _credentials_view = new CredentialsView((Storage)data.getSerializableExtra("storage"));
        Refresh();
      }
      else
      {
        if (_credentials_view == null) // If storage configuration has been cancelled -> exit
        {
          finish();
        }
      }
    }
  }

  private void OnFollowSymlink(String domain)
  {
    Optional<Credentials> credentials =  _credentials_view.GetAll().stream().filter(new Predicate<Credentials>()
    {
      @Override
      public boolean test(Credentials credentials)
      {
        return credentials.Domain().equals(domain);
      }
    }).findFirst();

    if (credentials.isPresent())
    {
      // Call new activity to examine the credentials
      Intent intent = new Intent(this, CredendialsViewActivity.class);
      intent.putExtra("credentials", credentials.get());
      intent.putExtra("create_new", false);

      startActivityForResult(intent, Flows.DisplayCredentials.ordinal());    }
  }

  private void OnCredentialsDisplayResult(CredendialsViewActivity.Result result, Intent data)
  {
    if (result == CredendialsViewActivity.Result.Cancelled) // User cancelled, do nothing
    {
      return;
    }

    try
    {
      if (result == CredendialsViewActivity.Result.Saved) // Modification saved, update storage
      {
        OnCredentialsSaved((Credentials)data.getSerializableExtra("old_credentials"),
                           (Credentials)data.getSerializableExtra("new_credentials"));
      }
      else if (result == CredendialsViewActivity.Result.Deleted) // Credentials deleted, update storage
      {
        OnCredentialsDeleted((Credentials)data.getSerializableExtra("old_credentials"));
      }
      else if (result == CredendialsViewActivity.Result.Created)
      {
        OnCredentialsCreated((Credentials)data.getSerializableExtra("new_credentials"));
      }
      else if (result == CredendialsViewActivity.Result.FollowSymlink)
      {
        OnFollowSymlink(data.getStringExtra("domain"));
      }
    }
    catch (Exception e)
    {
      AlertDialog.Builder builder  = new AlertDialog.Builder(this);
      builder.setMessage(e.toString());
      builder.setTitle("Failed to save credentials");
      builder.setPositiveButton("OK", null);
      builder.setCancelable(true);
      builder.create().show();
    }
  }

  private void OnCredentialsSaved(Credentials old_credentials, Credentials new_credentials)
  {
    Log.i("main", "New credentials saved");

    _credentials_view.UpdateCredentials(old_credentials, new_credentials);
  }

  private void OnCredentialsDeleted(Credentials credentials)
  {
    Log.i("main", "Credentials deleted");

    _credentials_view.DeleteCredentials(credentials);
  }

  private void ConfirmOverwrite(final Credentials credentials)
  {
    new AlertDialog.Builder(this)
            .setMessage("Credentials for : \"" + credentials.Domain() + "\"  already exist. Overwrite ? ")
            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
            {
              @Override
              public void onClick(DialogInterface dialog, int which)
              {
                _credentials_view.AddCredentials(credentials, true);
                dialog.dismiss();
              }
            })
            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener()
            {
              @Override
              public void onClick(DialogInterface dialog, int which)
              {
                dialog.dismiss();
              }
            })
            .create().show();
  }

  private void OnCredentialsCreated(Credentials credentials)
  {
    Log.i("main", "Credentials created");

    if (!_credentials_view.AddCredentials(credentials, false))
    {
      ConfirmOverwrite(credentials);
    }

    Refresh();
  }

  private void Refresh()
  {
    String filter = _filter.getText().toString();

    _adapter.SetCredentials(filter.isEmpty() && false? _credentials_view.GetAll() : _credentials_view.GetFilteredView(filter));
    _adapter.notifyDataSetChanged();
  }

  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after)
  {
  }

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count)
  {
    Refresh();
  }

  @Override
  public void afterTextChanged(Editable s)
  {
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id)
  {
    // Recover credentials object from view
    Credentials credentials = (Credentials)view.getTag();

    // Call new activity to examine the credentials
    Intent intent = new Intent(this, CredendialsViewActivity.class);
    intent.putExtra("credentials", credentials);
    intent.putExtra("create_new", false);


    startActivityForResult(intent, Flows.DisplayCredentials.ordinal());
  }

  private void CreateCredentials()
  {
    Intent intent = new Intent(this, CredendialsViewActivity.class);
    intent.putExtra("create_new", true);

    startActivityForResult(intent, Flows.DisplayCredentials.ordinal());
  }

  private void ConfigureStorage()
  {
    if (_credentials_view != null)
    {
      try
      {
        _credentials_view.close();
      }
      catch (Exception e)
      {
        Log.e("main", "Error while closing storage view: " + e);
      }

      _credentials_view = null;
    }

    Intent intent = new Intent(this, StorageConfigurationActivity.class);
    startActivityForResult(intent, Flows.ConfigureStorage.ordinal());
  }
}
