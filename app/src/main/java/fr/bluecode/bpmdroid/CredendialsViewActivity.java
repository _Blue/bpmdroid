package fr.bluecode.bpmdroid;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAmount;

import fr.bluecode.bpmdroid.View.TimeUtils;
import fr.bluecode.bpmdroid.storage.Credentials;
import fr.bluecode.bpmdroid.storage.Storage;

public class CredendialsViewActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener
{
  public enum Result
  {
    Cancelled,
    Saved,
    Deleted,
    Created,
    FollowSymlink
  }

  private EditText _domain;
  private EditText _username;
  private EditText _password;
  private CheckBox _show_password_checkbox;
  private Credentials _original_credentials;
  private TextView _last_edited;
  private Button _save;
  private Button _back;
  private Button _delete;

  private TextView _symlink_label;
  private boolean _create_new;

  private LocalDateTime _pause_ts = null;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_credendials_view);

    Intent intent = getIntent();
    _create_new = intent.getBooleanExtra("create_new", true);

    if (!_create_new)
    {
      _original_credentials = (Credentials)intent.getSerializableExtra("credentials");
    }
    SetupControls();
  }

  @Override
  protected void onResume()
  {
    super.onResume();
    _pause_ts = null;

    // Time out after 10 minutes
    if (_pause_ts != null && _pause_ts.isBefore(LocalDateTime.now().plusMinutes(10)))
    {
      Log.i("CredentialView", "Expiration ts exceeded, closing");
      finish();
    }
  }

  @Override
  protected void onPause()
  {
    assert(_pause_ts == null);

    _pause_ts = LocalDateTime.now();
    super.onPause();
  }

  private void SetupControls()
  {
    _domain = findViewById(R.id.credentials_view_domain);
    _username = findViewById(R.id.credentials_view_username);
    _password = findViewById(R.id.credentials_view_password);
    _last_edited = findViewById(R.id.credentials_view_last_edited);
    _delete = findViewById(R.id.credentials_view_delete);
    _show_password_checkbox = findViewById(R.id.credentials_view_password_checkbox);
    _save = findViewById(R.id.credentials_view_save);
    _back = findViewById(R.id.credentials_view_back);
    _symlink_label = findViewById(R.id.symlinkLabel);

    if (_original_credentials != null && _original_credentials.Symlink())
    {
      _symlink_label.setVisibility(View.VISIBLE);
      _symlink_label.setText("Symlink to : " + _original_credentials.Password());
      _symlink_label.setOnClickListener(new View.OnClickListener()
      {
        @Override
        public void onClick(View v)
        {
          Intent result = new Intent();
          result.putExtra("domain", _original_credentials.Password());
          setResult(Result.FollowSymlink.ordinal(), result);
          finish();
        }
      });
    }

    findViewById(R.id.domain_label).setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        OnLabelClicked(_domain);
      }
    });

    findViewById(R.id.username_label).setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        OnLabelClicked(_username);
      }
    });

    findViewById(R.id.password_label).setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        OnLabelClicked(_password);
      }
    });

    _domain.addTextChangedListener(new TextWatcher()
    {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after)
      {
      }

      @Override
      public void onTextChanged(CharSequence text, int start, int before, int count)
      {
        OnUrlEdited(text.toString());
      }

      @Override
      public void afterTextChanged(Editable s)
      {
      }
    });

    if (_create_new)
    {
      _password.setVisibility(View.VISIBLE);
      _show_password_checkbox.setVisibility(View.INVISIBLE);
      _delete.setVisibility(View.INVISIBLE);
      _save.setEnabled(false);
    }
    else
    {
      _show_password_checkbox.setOnCheckedChangeListener(this);
      _domain.setText(_original_credentials.Domain());
      _username.setText(_original_credentials.Username());
      _password.setText(_original_credentials.Password());

      _last_edited.setText("Last edited: " + TimeUtils.Format(_original_credentials.Timestamp()));
      _delete.setOnClickListener(new View.OnClickListener()
      {
        @Override
        public void onClick(View v)
        {
          Delete();
        }
      });
    }

    _back.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        setResult(Result.Cancelled.ordinal());
        finish();
      }
    });

    _save.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        Save();
      }
    });
  }

  private void OnLabelClicked(EditText text)
  {
    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
    clipboard.setPrimaryClip(ClipData.newPlainText("bpm", text.getText()));

    Toast.makeText(this, "Copied !", Toast.LENGTH_LONG).show();
  }

  private void OnUrlEdited(String url)
  {
    _save.setEnabled(!url.isEmpty());
    if (_create_new && !url.isEmpty())
    {
      _last_edited.setText("Computed domain name: \"" + Storage.GetAuthorityFromUrl(url) + "\"");
    }
  }

  private void Delete()
  {
    AlertDialog dialog = new AlertDialog.Builder(this)
            .setMessage("Delete credentials for domain: \"" + _original_credentials.Domain() + "\" ? ")
            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
            {
              @Override
              public void onClick(DialogInterface dialog, int which)
              {
                dialog.dismiss();
                OnDeleted();
              }
            })
            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener()
            {
              @Override
              public void onClick(DialogInterface dialog, int which)
              {
                dialog.dismiss();
              }
            })
            .create();

    dialog.show();
  }

  private void OnDeleted()
  {
    Intent intent = new Intent();
    intent.putExtra("old_credentials", _original_credentials);

    setResult(Result.Deleted.ordinal(), intent);
    finish();
  }

  private void Save()
  {
    Credentials new_credentials = new Credentials(Storage.GetAuthorityFromUrl(_domain.getText().toString()),
                                                  _username.getText().toString(),
                                                  _password.getText().toString(),
                                                  TimeUtils.Now(),
                                                  false);

    Intent intent = new Intent();
    intent.putExtra("old_credentials", _original_credentials);
    intent.putExtra("new_credentials", new_credentials);

    setResult(_create_new ? Result.Created.ordinal() : Result.Saved.ordinal(), intent);
    finish();
  }

  @Override
  public void onCheckedChanged(CompoundButton buttonView, boolean checked)
  {
    _password.setVisibility(checked ? View.VISIBLE : View.INVISIBLE);
  }
}
