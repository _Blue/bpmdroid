package fr.bluecode.bpmdroid;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Options
{
  private SharedPreferences _preferences;
  private static String _storage_key =  "storage_path";


  public Options(Context context)
  {
    _preferences = PreferenceManager.getDefaultSharedPreferences(context);
  }

  public boolean HasStoragePathSet()
  {
    return _preferences.contains(_storage_key);
  }

  public String GetStoragePath()
  {
    String path = _preferences.getString(_storage_key, null);

    if (path == null)
    {
      throw new RuntimeException("GetStoragePath called when storage_path is not set");
    }

    return path;
  }

  public void SetStoragePath(String path)
  {
    SharedPreferences.Editor editor = _preferences.edit();

    editor.putString(_storage_key, path);

    editor.apply();
  }
}
