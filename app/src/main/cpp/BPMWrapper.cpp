#include <jni.h>
#include <string>
#include <fstream>
#include <memory>
#include <algorithm>
#include <functional>
#include <BPMException.h>
#include <assert.h>
#include "Helper.h"
#include "Defs.h"
#include "data/SecuredStorage.h"
#include "JNIHelper.h"

using namespace bpm;
using namespace bpm::data;

Storage* GetStorage(JNIEnv *env, jobject instance)
{
  auto object_class = env->GetObjectClass(instance);
  auto field_id = env->GetFieldID(object_class, "_storage", "J");
  auto field_value = env->GetLongField(instance, field_id);

  assert(field_value != 0);

  return reinterpret_cast<SecuredStorage*>(field_value);
}

extern "C" JNIEXPORT jlong JNICALL
Java_fr_bluecode_bpmdroid_storage_Storage_CreateImpl(JNIEnv *env, jobject instance, jstring path,
                                                     jstring password)
{
  JNIHelper helper(env);
  std::ifstream file(helper.Convert(path));
  if (!file)
  {
    std::string error = "Failed to open storage file, errno = " + std::to_string(errno);
    return env->ThrowNew(env->FindClass("java/lang/Exception"), error.c_str());
  }

  auto storage = std::make_unique<SecuredStorage>(helper.Convert(path),
                                                  static_cast<size_t>(defs::default_encryption_key_size),
                                                  helper.ConvertSecure(password));

  try
  {
    if (!storage->Good())
    {
      return env->ThrowNew(env->FindClass("java/lang/Exception"),
                           "Failed to unlock storage: incorrect password");
    }
  }
  catch (const bpm::BPMException& e)
  {
    std::string message = "Failed to open storage: " + std::string(e.what());
    return env->ThrowNew(env->FindClass("java/lang/Exception"), message.c_str());
  }

  // Fairly ugly, but no easy way to have RAII in java
  return reinterpret_cast<long>(storage.release());
}


extern "C" JNIEXPORT jobjectArray JNICALL
Java_fr_bluecode_bpmdroid_storage_Storage_GetCredentialsImpl(JNIEnv *env, jobject instance)
{
  JNIHelper helper(env);
  auto storage = GetStorage(env, instance);

  return helper.Convert(storage->GetCredentials());
}

extern "C" JNIEXPORT void JNICALL
Java_fr_bluecode_bpmdroid_storage_Storage_ReleaseImpl(JNIEnv *env, jobject instance)
{
  auto storage = GetStorage(env, instance);
  delete storage;
}

extern "C" JNIEXPORT jboolean JNICALL
Java_fr_bluecode_bpmdroid_storage_Storage_AddCredentials(JNIEnv *env,
                       jobject instance,
                       jstring domain,
                       jstring username,
                       jstring password,
                       jlong ts,
                       jboolean overwrite)
{
  JNIHelper helper(env);
  auto storage = GetStorage(env, instance);

  try
  {
    return helper.Convert(storage->AddCred(helper.ConvertSecure(domain), helper.ConvertSecure(username), helper.ConvertSecure(password), ts, overwrite));
  }
  catch(const std::exception& e)
  {
    env->ThrowNew(env->FindClass("java/lang/Exception"), e.what());
    return 0;
  }
}

extern "C" JNIEXPORT void JNICALL
Java_fr_bluecode_bpmdroid_storage_Storage_EditDomain(JNIEnv *env,
                                                     jobject instance,
                                                     jstring old_domain,
                                                     jstring new_domain,
                                                     jlong ts)
{
  JNIHelper helper(env);
  auto storage = GetStorage(env, instance);

  storage->ChangeDomain(helper.ConvertSecure(old_domain), helper.ConvertSecure(new_domain), ts);
}

extern "C" JNIEXPORT void JNICALL
Java_fr_bluecode_bpmdroid_storage_Storage_EditUsername(JNIEnv *env,
                                                           jobject instance,
                                                           jstring domain,
                                                           jstring new_username,
                                                           jlong ts)
{
  JNIHelper helper(env);
  auto storage = GetStorage(env, instance);

  storage->ChangeUsername(helper.ConvertSecure(domain), helper.ConvertSecure(new_username), ts);
}

extern "C" JNIEXPORT void JNICALL
Java_fr_bluecode_bpmdroid_storage_Storage_EditPassword(JNIEnv *env,
                                                           jobject instance,
                                                           jstring domain,
                                                           jstring new_password,
                                                           jlong ts)
{
  JNIHelper helper(env);
  auto storage = GetStorage(env, instance);

  storage->ChangePassword(helper.ConvertSecure(domain), helper.ConvertSecure(new_password), ts);
}

extern "C" JNIEXPORT void JNICALL
Java_fr_bluecode_bpmdroid_storage_Storage_DeleteCredentials(JNIEnv *env, jobject instance, jstring domain)
{
  JNIHelper helper(env);
  auto storage = GetStorage(env, instance);

  storage->DeleteCred(helper.ConvertSecure(domain));
}

extern "C" JNIEXPORT jstring JNICALL
Java_fr_bluecode_bpmdroid_storage_Storage_GetAuthorityFromUrl(JNIEnv *env, jclass type, jstring url)
{
  JNIHelper helper(env);

  return helper.Convert(bpm::Helper::GetDomainAuthority(helper.Convert(url)));
}