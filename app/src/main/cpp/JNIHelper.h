//
// Created by blue on 10/28/18.
//

#pragma once

#include <string>
#include <jni.h>
#include "data/Credentials.h"
#include "data/SecuredString.h"

class JNIHelper
{
public:
  JNIHelper(JNIEnv *env);

  std::string Convert(jstring input);

  bool Convert(jboolean input);

  bpm::data::SecuredString ConvertSecure(jstring input);

  jboolean Convert(bool input);

  jlong Convert(size_t input);

  jstring Convert(const char *input);

  jstring Convert(const bpm::data::SecuredString &input);

  jobject Convert(const bpm::data::Credentials &input);

  template<typename T>
  jobjectArray Convert(const std::vector<T> &input)
  {
    auto jni_class = _env->FindClass(GetJNIClassName<T>().c_str());
    auto array = _env->NewObjectArray(input.size(), jni_class, nullptr);

    for (size_t i = 0; i < input.size(); i++)
    {
      _env->SetObjectArrayElement(array, i, Convert(input[i]));
    }

    return array;
  }


private:
  template<typename T>
  static std::string GetJNIClassName();


private:
  JNIEnv *_env = nullptr;
};