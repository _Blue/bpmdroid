//
// Created by blue on 10/28/18.
//

#include "JNIHelper.h"
#include "data/SecuredStorage.h"

JNIHelper::JNIHelper(JNIEnv *env) : _env(env)
{
}

template <typename T>
std::string JNIHelper::GetJNIClassName()
{
  static_assert(sizeof(T) != sizeof(T), "JNI class name not found");
}

template <>
std::string JNIHelper::GetJNIClassName<bpm::data::Credentials>()
{
  return "fr/bluecode/bpmdroid/storage/Credentials";
}

std::string JNIHelper::Convert(jstring input)
{
  auto content =  _env->GetStringUTFChars(input, 0);

  std::string output(content);
  _env->ReleaseStringUTFChars(input, content);

  return output;
}

bpm::data::SecuredString JNIHelper::ConvertSecure(jstring input)
{
  auto content =  _env->GetStringUTFChars(input, 0);

  bpm::data::SecuredString output(content);
  _env->ReleaseStringUTFChars(input, content);

  return output;
}

jboolean JNIHelper::Convert(bool input)
{
  return static_cast<jboolean>(input);
}

jstring JNIHelper::Convert(const std::string &input)
{
  return Convert(input.c_str());
}

jstring JNIHelper::Convert(const char *input)
{
  return _env->NewStringUTF(input);
}

jlong JNIHelper::Convert(size_t input)
{
  return input;
}

bool JNIHelper::Convert(jboolean input)
{
  return static_cast<bool>(input);
}

jobject JNIHelper::Convert(const bpm::data::Credentials &input)
{
  auto jni_class = _env->FindClass(GetJNIClassName<bpm::data::Credentials>().c_str());
  auto ctor = _env->GetMethodID(jni_class, "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V");

  return _env->NewObject(jni_class, ctor, Convert(input.domain), Convert(input.username), Convert(input.password), Convert(input.ts), Convert(bpm::data::Storage::IsSymlink(input.username)));
}



