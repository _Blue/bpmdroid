# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

# Sets the minimum version of CMake required to build the native library.

cmake_minimum_required(VERSION 3.4.1)

# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds them for you.
# Gradle automatically packages shared libraries with your APK.
set(CMAKE_ANDROID_STL_TYPE c++_shared)
add_library( # Sets the name of the library.
             native-lib

             # Sets the library as a shared library.
             SHARED

             # Provides a relative path to your source file(s).
             src/main/cpp/BPMWrapper.cpp
             src/main/cpp/JNIHelper.cpp )



# Searches for a specified prebuilt library and stores the path as a
# variable. Because CMake includes system libraries in the search path by
# default, you only need to specify the name of the public NDK library
# you want to add. CMake verifies that the library exists before
# completing its build.
add_library(bpm STATIC IMPORTED)
add_library(cryptopp STATIC IMPORTED)
add_library(c++_shared STATIC IMPORTED)
add_library(m STATIC IMPORTED)
add_library(c STATIC IMPORTED)
add_library(dl STATIC IMPORTED)
set_target_properties(bpm PROPERTIES IMPORTED_LOCATION ${CMAKE_SOURCE_DIR}/jniLibs/${ANDROID_ABI}/libbpm.so)
set_target_properties(cryptopp PROPERTIES IMPORTED_LOCATION ${CMAKE_SOURCE_DIR}/jniLibs/${ANDROID_ABI}/libcryptopp.so)
set_target_properties(m PROPERTIES IMPORTED_LOCATION ${CMAKE_SOURCE_DIR}/jniLibs/${ANDROID_ABI}/libm.so)
set_target_properties(c PROPERTIES IMPORTED_LOCATION ${CMAKE_SOURCE_DIR}/jniLibs/${ANDROID_ABI}/libc.so)
set_target_properties(dl PROPERTIES IMPORTED_LOCATION ${CMAKE_SOURCE_DIR}/jniLibs/${ANDROID_ABI}/libdl.so)
set_target_properties(c++_shared PROPERTIES IMPORTED_LOCATION ${CMAKE_SOURCE_DIR}/jniLibs/${ANDROID_ABI}/libc++_shared.so)

find_library( # Sets the name of the path variable.
              log-lib

              # Specifies the name of the NDK library that
              # you want CMake to locate.
              log )

link_directories(/home/blue/repos/bluepasswordmanager/lib)


# Specifies libraries CMake should link to your target library. You
# can link multiple libraries, such as libraries you define in this
# build script, prebuilt third-party libraries, or system libraries.

target_link_libraries( # Specifies the target library.
                       native-lib
                       bpm
                       cryptopp
                       c++_shared
                       m
                       dl
                       c
                       # Links the target library to the log library
                       # included in the NDK.
                        ${log-lib})

add_definitions(-DANDROID)

include_directories(../deps/BluePasswordManager/src/core)
